import { Pannellum } from "pannellum-react"
import React, { useRef, useState, useEffect } from "react";
import { TextField, Drawer, Button } from '@material-ui/core';
import { makeStyles, createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';

import panoImageSrc from './pano-images/IMG_0124.jpg'

const theme = createMuiTheme({
    palette: {
      primary: {
        main: '#e83f2e',
      },
      secondary: {
        main: '#2a3571',
      },
      tonalOffset: 0.1,
    },
  });
  
const useStyles = makeStyles((theme) => ({
    root: {
      '& .MuiTextField-root': {
        margin: theme.spacing(2),
        width: '25ch',
        display: 'flex',
      },
    },
    title: {
      margin: theme.spacing(2),
    },
    button: {
      margin: theme.spacing(2),
    },
    input: {
      display: 'none',
    },  
}));
const Json = [
    {
        "type": "custom",
        "pitch": -10.061414181110537,
        "yaw": -2.27127993353921,
        "hfov": 150,
        "tooltip": "hotspotDisplay1",
        "tooltipArg": "My hotspot 1",
        "poi_files": [
            {
                "file_id": 0,
                "file_name": "",
                "file_type": "",
                "file_path": ""
            },
            {
                "file_id": 1,
                "file_name": "",
                "file_type": "",
                "file_path": ""
            }
        ]
    },
    {
        "type": "custom",
        "pitch":  2.9041245428623563,
        "yaw":  43.102330931646044,
        "hfov": 150,
        "tooltip":"hotspotDisplay2",
        "tooltipArg": "My hotspot 2"
    },
    {
        "type": "custom",
        "pitch": 2.0380524407578284,
        "yaw": 51.77017113892216,
        "hfov": 150,
        "tooltip": "hotspotDisplay3",
        "tooltipArg": "My hotspot 3"
    }
]

function Panorama() { 

    var poiArray = []

    const classes = useStyles();

    const [yaw, setYaw] = useState(0);
    const [pitch, setPitch] = useState(0);
    const [poiTitle, setPoiTitle] = useState("");
    const [poiInfo, setPoiInfo] = useState("");
    const [poiInputData, setPoiInputData] = useState({
        poiTitle: poiTitle,
        pitch: pitch,
        yaw: yaw,
        poiInfo: poiInfo,
        /*poi_files: [
            {
                file_name: '',
                file_type: '',
                file_path: ''  
            }
        ]*/
    })
    const [jsonData, setJsonData] = useState(Json)
    const [poiData, setPoiData] = useState(poiArray);
    const [open, setOpen] = useState(false)
    const [files, setFiles] = useState([])

    const panImage = useRef(null);   

        jsonData.map(poi => {
            poiArray.push(<Pannellum.Hotspot
                id= {poi.pitch}
                key={poi.pitch}
                type= {poi.type}
                pitch={poi.pitch}
                yaw={poi.yaw}
                hfov={poi.hfov}
                text={poi.tooltip}
                //URL="https://github.com/farminf"
                handleClick= {(e, args) => showInformationen(args)}
                handleClickArg={
                    {
                        poiTitle: poi.tooltipArg,
                        pitch: poi.pitch,
                        yaw: poi.yaw,
                        poi_files:  //poi_files.map(file => {})
                            [
                                {
                                    file_name: 'Photo1.jpg', //
                                    file_type: 'image',
                                    file_path: './Photo1.jpg'},
                                {
                                    file_name: 'Pdf1.pdf',
                                    file_type: 'pdf',
                                    file_path: './Pdf1.pdf'}
                            ],
                            //map files 
                        info: poi.tooltip
                    }
                }
                cssClass="custom-hotspot"
                //tooltip={hotspot}
                //tooltipArg="Hotspot 1"
                />) 
        })
    
    function showInformationen(handleClickArgs) {
        console.log("test",handleClickArgs)
        setPoiTitle(handleClickArgs.poiTitle)
        setPitch(handleClickArgs.pitch)
        setYaw(handleClickArgs.yaw)
        setPoiInfo(handleClickArgs.info)
        setFiles(handleClickArgs.poi_files)
        console.log("data before", jsonData)
        //console.log(JSON.stringify(jsonData))
        //updateArray(handleClickArgs.pitch, handleClickArgs.info,handleClickArgs.poiTitle )
        setOpen(true)
        //TODO: addPoiFiles(handleClickArgs.poi_files) : map poi_files 
    }

    /*
    function addPoiFiles(poiFiles, file) {
        poiFiles.push(file)
    }
*/
    
    function handleChange(event) {
        const {name, value} = event.target
        setPoiInputData(prevInputData => ({...prevInputData, [name]: value}))
        console.log(poiInputData)
    }

    const closeDrawer = () => {
        setOpen(false)
    }

    function handleSubmit(event) {
        event.preventDefault()  
        //wird nur ein Inputfeld überschrieben, kommt das nicht bearbeitete vom State 
        updateArray(pitch, poiInputData.poiInfo, poiInputData.poiTitle)
        closeDrawer()
    }

    //updateArray() TODO: 
    //-generalisieren: Json-Objekt als param übergeben
    //-vergleich pitch & yaw statt nur pitch?

    function updateArray(pitch, info, title) {
        var foundIndex = Json.findIndex(x => x.pitch === pitch)
        if(foundIndex !== -1) {
            Json[foundIndex].tooltip = info
            Json[foundIndex].tooltipArg = title


            //was machen, wenn sich nur eins der beiden ändert? Im Moment wird das Feld, das sich nicht
            //geändert hat, geleert, da inputData übernommen wird 
            //Lösung todo: generalisieren 

            console.log("poiArray", poiArray)
        } else {
            pushToJson(info, title)
        }        
        //console.log("JsonIndex",Json[foundIndex])
    }

    function pushToJson(poiInfoIO, poiTitleIO) {
        Json.push(
            {
                type: "custom",
                pitch: pitch,
                yaw: yaw,
                hfov: 150,
                tooltip: poiInfoIO,
                tooltipArg: poiTitleIO 
            }
        )
        console.log("json",Json)
    }
    
    
    useEffect(() => { 
        setPoiData(poiArray)    
        console.log("poiArray", poiArray)
      }, [poiArray.length]);


    //funktioniert, aber Seite lädt nicht neu   
    useEffect(() => {      
        setPoiData(poiArray) 
        console.log("poiArray-2", poiArray)    
    },[JSON.stringify(jsonData)]);


    return (
        <>   
        <div className='coord-info'>
                <div> Pitch: {pitch} </div>
                <div> Yaw: {yaw} </div>
        </div> 
        <ThemeProvider theme={theme}>    
        <Drawer anchor='right' open={open} onClose={closeDrawer}>
            <div className="form-container">
                <form className={classes.root} noValidate autoComplete="off" onSubmit={handleSubmit}>
                    <div>
                        <TextField 
                            id="outlined-basic" 
                            label="Titel" 
                            placeholder="Add POI title here"
                            variant="outlined" 
                            defaultValue={poiTitle}
                            name="poiTitle"
                            onChange={handleChange}
                        />
                        <TextField
                            id="outlined-read-only-input"
                            label="Pitch"
                            value={pitch}
                            InputProps={{
                                readOnly: true,
                            }}
                            variant="outlined"
                            name="pitch"
                        />
                        <TextField
                            id="outlined-read-only-input"
                            label="Yaw"
                            value={yaw}
                            InputProps={{
                                readOnly: true,
                            }}
                            variant="outlined"
                            name="yaw"
                        />
                        <TextField 
                            id="outlined-multiline-static"
                            label="Info" 
                            placeholder="Add POI Information here"
                            multiline
                            rows={2}
                            defaultValue={poiInfo}
                            variant="outlined" 
                            name="poiInfo"
                            onChange={handleChange}
                        />
                        <p>
                            <input
                                accept="image/*"
                                className={classes.input}
                                id="contained-button-file"
                                multiple
                                type="file"
                            />
                            <label htmlFor="contained-button-file">
                                <Button variant="contained" color="secondary" component="span" className={classes.button}>
                                    Upload
                                </Button>
                            </label>
                        </p>
                        <Button type="submit" variant="outlined" className={classes.button}>
                            Save Hotspot
                        </Button>
                    </div>
                </form>
            </div>
            {/*<div className={classes.image}></div>*/}
        </Drawer>
        </ThemeProvider>   
            {createPannellumViewer()}            
        </>
    );

    function createPannellumViewer() {
        return(
            <div className='panorama'>
                <Pannellum
                    ref={panImage}
                    id='pannellum'
                    width='100%'
                    height='30em'
                    image= {panoImageSrc}
                    pitch={5}
                    yaw={0}
                    hfov={200}
                    author='Lumoview Building Analytics GmbH'
                    showZoomCtrl={true}
                    hotspotDebug={false}
                    onLoad={() => {
                        console.log("panorama loaded");
                        //console.log(panImage.current);
                    }}
                    autoLoad
                    onMouseup={(event)=> {createNewHotspot(event)}}
                    

                    //onMousedown={(evt)=>{addHotspot(evt);}}
                    //onMousedown={(evt)=>{console.log("Mouse Down", evt);}}
                    //onMouseup={(evt)=>{console.log("Mouse Up", evt);}}                   
                > 
                    {poiData}
                </Pannellum>
            </div>   
        )
    };

    function createNewHotspot(e) {
        if(e.which === 3) {
            setPoiTitle(null)
            setPoiInfo(null)
            hotspotCoords(e)
            setOpen(true)
        }
    }

    function hotspotCoords(e) {
        if(e.which === 3) {
            setPitch(panImage.current.getViewer().mouseEventToCoords(e)[0])
            setYaw(panImage.current.getViewer().mouseEventToCoords(e)[1])
            //console.log("Right mouse button was clicked and Hotspot is added at position:\n\n" +
            //"Pitch: " + panImage.current.getViewer().mouseEventToCoords(e)[0]+ "\nYaw: " + panImage.current.getViewer().mouseEventToCoords(e)[1])
        }
    };

}

export default Panorama